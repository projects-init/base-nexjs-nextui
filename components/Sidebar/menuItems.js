export const menuItems = [
    {
        id:'menu1',
        path: '/admin/dashboard',
        icon: 'fas fa-tv',
        title: 'Dashboard',
        role: []
    },
    {
        id:'menu2',
        path: '/admin/settings',
        icon: 'fas fa-tv',
        title: 'Settings',
        role: []
    }
]