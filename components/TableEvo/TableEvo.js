import React, { useCallback } from "react";
import {
  Table,
  TableHeader,
  TableColumn,
  TableBody,
  TableRow,
  TableCell,
  getKeyValue,
  Pagination,
} from "@nextui-org/react";
import PropTypes from "prop-types";

export default function TableEvo({
  columns,
  data,
  pageIndex,
  pageSize,
  total,
  disabledKeys,
  selectedKeys,
  hasPagination,
  selectionBehavior,
  setRowsPerPage,
  onSelectionChange,
  onChangePage,
}) {
  const pages = Math.ceil(total / pageSize);

  const onRowsPerPageChange = useCallback((e) => {
    setRowsPerPage(Number(e.target.value));
  }, []);

  const bottomContent = React.useMemo(() => {
    return (
      <div className="py-2 px-2 flex justify-between items-center">
        {selectionBehavior == "toggle" && (
          <span className="w-[30%] text-small text-default-400">
            {selectedKeys === "all"
              ? "All items selected"
              : `${selectedKeys?.size} of ${total} selected`}
          </span>
        )}

        <Pagination
          isCompact
          showControls
          showShadow
          color="primary"
          page={pageIndex}
          total={pages}
          onChange={(page) => onChangePage(page)}
        />
      </div>
    );
  }, [selectedKeys, total, pageIndex, pages]);

  const renderCell = React.useCallback(
    (data, columnKey) => {
      const cellValue = data[columnKey];
      const column = columns.find((col) => col.key == columnKey);

      if(column?.render) {
        return <>{column?.render(data)}</>;
      }
      return cellValue;
    },
    [columns, data]
  );
  return (
    <Table
      aria-label="Controlled table example with dynamic content"
      color="primary"
      selectionMode="multiple"
      disabledKeys={disabledKeys}
      selectionBehavior={selectionBehavior}
      selectedKeys={selectedKeys}
      onSelectionChange={onSelectionChange}
      bottomContent={hasPagination ? bottomContent : null}
    >
      <TableHeader columns={columns}>
        {(column) => (
          <TableColumn key={column.key}>
            {column.label}
          </TableColumn>
        )}
      </TableHeader>
      {data?.length ? (
        <TableBody items={data}>
          {(item) => (
            <TableRow key={item.key}>
              {(columnKey) => (
                <TableCell>{renderCell(item, columnKey)}</TableCell>
              )}
            </TableRow>
          )}
        </TableBody>
      ) : (
        <TableBody emptyContent={"No rows to display."}>{[]}</TableBody>
      )}
    </Table>
  );
}

TableEvo.defaultProps = {
  disabledKeys: [],
  selectionBehavior: "toggle",
  setSelectedKeys: [],
  hasPagination: true,
  total: 0,
  pageIndex: 1,
  pageSize: 10,
  onChangePage: () => {},
};

TableEvo.propTypes = {
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  selectionBehavior: PropTypes.PropTypes.oneOf(["toggle", "replace"]),
};
