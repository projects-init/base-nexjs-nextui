import { withAuth } from "next-auth/middleware";
import { NextResponse } from "next/server";

export default withAuth(
  // `withAuth` augments your `Request` with the user's token.
  function middleware(req) {
    console.log("token: ", req.nextauth.token);
    if (req.nextUrl.pathname.startsWith("/admin") && req.nextauth.token?.role !== "admin")
      return NextResponse.rewrite(
        new URL("/status/403", req.url)
      );
    if (req.nextUrl.pathname.startsWith("/user") && req.nextauth.token?.role !== "user")
      return NextResponse.rewrite(
        new URL("/auth/login?message=You Are Not Authorized!", req.url)
      );
  },
  {
    callbacks: {
      authorized: ({ token }) => !!token,
    },
    secret: process.env.NextAuth_SECRET,

  }
);

export const config = {
  matcher: ["/admin/:path*", "/user/:path*"],
};