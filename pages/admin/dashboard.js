import React, { useState } from "react";
import { Tooltip } from "@nextui-org/react";
// components

import CardLineChart from "../../components/Cards/CardLineChart.js";
import CardBarChart from "../../components/Cards/CardBarChart.js";
import CardPageVisits from "../../components/Cards/CardPageVisits.js";
import CardSocialTraffic from "../../components/Cards/CardSocialTraffic.js";
import TableEvo from "../../components/TableEvo/TableEvo.js";

// layout for page

import Admin from "../../layouts/Admin.js";

const rows = [
  {
    key: "1",
    name: "Tony Reichert",
    role: "CEO",
    status: "Active",
  },
  {
    key: "2",
    name: "Zoey Lang",
    role: "Technical Lead",
    status: "Paused",
  },
  {
    key: "3",
    name: "Jane Fisher",
    role: "Senior Developer",
    status: "Active",
  },
  {
    key: "4",
    name: "William Howard",
    role: "Community Manager",
    status: "Vacation",
  },
  {
    key: "5",
    name: "William Howard",
    role: "Community Manager",
    status: "Vacation",
  },
  {
    key: "6",
    name: "William Howard",
    role: "Community Manager",
    status: "Vacation",
  },
  {
    key: "7",
    name: "William Howard",
    role: "Community Manager",
    status: "Vacation",
  },
];

const columns = [
  {
    key: "name",
    label: "NAME",
  },
  {
    key: "role",
    label: "ROLE",
  },
  {
    key: "status",
    label: "STATUS",
  },
  {
    key: "action",
    label: "action",
    render: (cellValue) => {
      return (
        <>
          <div className="relative flex items-center gap-2">
            <Tooltip content="Details">
              <span className="text-lg text-default-400 cursor-pointer active:opacity-50" onClick={()=>console.log(cellValue)}>
              <i class="fa fa-eye" aria-hidden="true"></i>
              </span>
            </Tooltip>
            <Tooltip content="Edit user">
              <span className="text-lg text-default-400 cursor-pointer active:opacity-50" onClick={()=>console.log("edit")}>
              <i class="fa fa-edit" aria-hidden="true"></i>
              </span>
            </Tooltip>
            <Tooltip color="danger" content="Delete user">
              <span className="text-lg text-danger cursor-pointer active:opacity-50" onClick={()=>console.log("remove")}>
              <i class="fa fa-trash" aria-hidden="true"></i>
              </span>
            </Tooltip>
          </div>
        </>
      );
    },
  },
];

export default function Dashboard() {
  const [query, setQuery] = useState({ pageIndex: 1, pageSize: 10 });
  const [selectedRows, setSelectedRows] = useState(new Set([]));
  return (
    <Admin>
      <div className="flex flex-wrap">
        <div className="w-full xl:w-8/12 mb-12 xl:mb-0 px-4">
          <CardLineChart />
        </div>
        <div className="w-full xl:w-4/12 px-4">
          <CardBarChart />
        </div>
      </div>
      <div className="flex flex-wrap mt-4">
        <div className="w-full xl:w-8/12 mb-12 xl:mb-0 px-4">
          <CardPageVisits />
        </div>
        <div className="w-full xl:w-4/12 px-4">
          <CardSocialTraffic />
        </div>
      </div>
      <TableEvo
        columns={columns}
        data={rows}
        pageIndex={query?.pageIndex}
        pageSize={query?.pageSize}
        total={rows?.length}
        selectedKeys={selectedRows}
        onSelectionChange={(selectedRow) => {
          setSelectedRows(selectedRow);
        }}
        onChangePage={(page) => setQuery({ pageIndex: page })}
        setRowsPerPage={(pageSize) =>
          setQuery({ pageSize: pageSize, pageIndex: 1 })
        }
      />
    </Admin>
  );
}
