import React from "react";

import "../styles/globals.css";
import "../styles/tailwind.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import Head from "next/head";
import { SessionProvider } from "next-auth/react";
import { NextUIProvider } from "@nextui-org/react";

export default function App({
  Component,
  pageProps: { session, ...pageProps },
}) {
  return (
    <React.Fragment>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <title>Evo NextJS</title>
      </Head>
      <SessionProvider session={session}>
        <NextUIProvider>
          <Component {...pageProps} />
        </NextUIProvider>
      </SessionProvider>
    </React.Fragment>
  );
}
