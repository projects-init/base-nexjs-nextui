import { signOut, useSession } from "next-auth/react";

export default function PermissionDeined() {
  return (
    <section className="grid h-screen place-items-center">
      <div className="w-25">
        <p>You do not have permission to view this page!</p>
      </div>
    </section>
  );
}
